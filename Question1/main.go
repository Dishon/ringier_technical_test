package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/gorilla/mux"

	"github.com/mackerelio/go-osstat/cpu"
	"github.com/mackerelio/go-osstat/memory"
)

type Data struct {
	Tech `json:"tech"`
}

type Tech struct {
	// since the type of returnvalue was not specified, it takes any type
	ReturnValue interface{} `json:"return_value"`
}

func main() {

	log.Println("Starting the HTTP server on port 8090")

	router := mux.NewRouter().StrictSlash(true)
	initialiseHandlers(router)
	log.Fatal(http.ListenAndServe(":8090", router))
}

func initialiseHandlers(router *mux.Router) {
	router.HandleFunc("/metrics", GetServerMetrics).Methods("GET")
	router.HandleFunc("/", GetValue).Methods("GET")
	router.HandleFunc("/", UpdateValue).Methods("POST")
}

// GET return_value
func GetValue(w http.ResponseWriter, r *http.Request) {
	// Open our jsonFile
	jsonFile, err := os.Open("tech_assess.json")
	// if we os.Open returns an error then handle it
	if err != nil {
		log.Println(err)
	}
	log.Println("Successfully Opened tech_assess.json")
	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)

	var data Data
	json.Unmarshal(byteValue, &data)

	log.Println(&data)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&data)
}

// GET server load
func GetServerMetrics(w http.ResponseWriter, r *http.Request) {

	resp := make(map[string]interface{})

	memory, memErr := memory.Get()
	if memErr != nil {
		fmt.Fprintf(os.Stderr, "%s\n", memErr)
		return
	}

	cpuUsage, err := cpu.Get()
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		return
	}
	fmt.Printf("cpu system: %f %%\n", float64(cpuUsage.System))
	// available memory
	resp["Available disk"] = strconv.FormatUint(memory.Free, 10) + " bytes"
	resp["Server load"] = cpuUsage.System

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(resp)
}

// POST request
func UpdateValue(w http.ResponseWriter, r *http.Request) {
	log.Println("Post request received")
	if r.Header.Get("Content-Type") != "application/json" {
		msg := "Content-Type header is not application/json"
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnsupportedMediaType)
		makeJson(msg, w, r)
		return
	}

	var data Data
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		msg := "Payload not a valid json"
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		makeJson(msg, w, r)
		return
	}

	log.Println(data)

	b, err := json.Marshal(data)
	if err != nil {
		log.Println(err)
	}
	// write to file to update return_value
	err = os.WriteFile("tech_assess.json", b, 0644)
	if err != nil {
		log.Println(err)
	}

	// set Content-Type
	w.Header().Set("Content-Type", "application/json")
	response := "return_value updated successfully"

	w.WriteHeader(http.StatusOK)

	makeJson(response, w, r)
}

// format response string to json format
func makeJson(response string, w http.ResponseWriter, r *http.Request) {
	resp := make(map[string]string)
	resp["message"] = response
	json.NewEncoder(w).Encode(resp)
}
