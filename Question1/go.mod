module example.com/m

go 1.17

require (
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/mackerelio/go-osstat v0.2.2 // indirect
	golang.org/x/sys v0.0.0-20220319134239-a9b59b0215f8 // indirect
)
