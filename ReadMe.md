### Question1
This is a rest Api with 3 endpoints

1. GET endpoint that return return_value
```bash
   curl --location --request GET 'http://localhost:8090/' --header 'Content-Type: application/json'
```

2. POST endpoint for updating return_value
```bash
    curl --location --request POST 'http://localhost:8090/' --header 'Content-Type: application/json' --data-raw '{"tech":{"return_value":1338}}' 
```
3. GET endpoint for return server load
```bash
    curl --location --request GET 'http://localhost:8090/metrics' --header 'Content-Type: application/json'
```

The applications is dockerised, use below steps to build anf run the docker image
```bash
    cd Question1
    docker build -t test0 .
    docker run -p 8090:8090 test0
```
To run without docker is `go run main.go`
test0 is docker image name that can be changed

### Question2
Set aws secret key and access ID using `aws configure` command then run the following commands
```bash
    terraform init
    terraform plan
    terraform apply -auto-approve 
```

### Question3
- Commented out, `"font-family:monospace"`  L13 of `main.js` file for the application to run
- To run the application make `docker-build.sh` executable by running  and then run the script
```bash
    cd Question3 && chmod +x docker-build.sh
    ./docker-build.sh
```
