variable "ubuntu_20_4" {
    type = string
    default = "ami-0b2537c6147faa3e2"
}

// Key-par name, please attach path to your public ssh key file
variable "keyname" {
    type = string
}

// variable to vpc id
variable "vpc" {
    type = string
}

// variable to subnet id
variable "subnetid" {
    type = string
}