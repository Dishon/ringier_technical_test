terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 4.5"
    }
  }

}

// a dummy role example 
resource "aws_iam_role" "ec2_test_access_role" {
  name               = "test-role"
  assume_role_policy = "${file("assumerolepolicy.json")}"
}

resource "aws_iam_instance_profile" "test_profile" {                             
    name  = "test_profile"                         
    role = "${aws_iam_role.ec2_test_access_role.name}"
}

resource "aws_instance" "test_instance" {
  ami = "${var.ubuntu_20_4}"
  instance_type = "t3.nano"
  key_name = var.keyname
  vpc_security_group_ids = [aws_security_group.allow_ssh.id]
  iam_instance_profile = "${aws_iam_instance_profile.test_profile.name}"
  subnet_id = var.subnetid

  tags = {
    Name = "test_instance"
  }
}

resource "aws_security_group" "allow_ssh" {
  name        = "ssh-sq"
  description = "Security group for ssh access"
  vpc_id = var.vpc
  ingress = [
    {
      description      = "Allow SSH accesss"
      cidr_blocks = ["0.0.0.0/0"]
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      security_groups  = []
      self             = false
      from_port        = 22
      to_port          = 22
      protocol         = "tcp"
    }
  ]
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
