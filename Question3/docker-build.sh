#/bin/bash

set -eu

DOCKER_IMAGE="test12"

# build docker image, already uses caching logic. If there is no need for caching logic we use --no-cache flag
DOCKER_BUILDKIT=1 docker build -t $DOCKER_IMAGE -f Dockerfile .

# run dcoker image, use --env FOOBAR="hey" for setting environment variable to be read 
docker run -d -p 8080:8080 --env FOOBAR="hey" $DOCKER_IMAGE 